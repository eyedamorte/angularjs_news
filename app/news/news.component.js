
let news = angular.module('news', [])
news.directive('appNews', function(){
  return{
    restrict: 'E',
    templateUrl: "news/news.component.html",
    link: function(scope, element, attrs){
      console.log('directive');
    }
  }
})


news.controller('newsCtrl', function($http, $scope){
  $scope.qry = 'anime';
  $scope.page = 1;
  $scope.inc = function(){
      $scope.page = $scope.page + 1;
      $scope.getData();
      console.log($scope.page);
  }

  $scope.dec = function(){
      if ($scope.page > 1) {
        $scope.page = $scope.page - 1;
        $scope.getData();
      }else{
        alert('нулевая страница');
      }
  }
  $scope.getData = function(){
    $scope.url = 'https://newsapi.org/v2/everything?pageSize=20&page=' + $scope.page + "&q=" + $scope.qry + '&apiKey=eed81886b1b8427d8f75b1aec5837517';
    $http.get($scope.url)
      .then((success)=>{
           console.log('success', success.data);
           $scope.articles = success.data.articles;
      },(error)=>{
           alert('последняя страница');
           $scope.page = $scope.page - 1;
           console.log('error');
      });
  }
  $scope.getData();

});;
