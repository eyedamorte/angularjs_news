let topNews = angular.module('topnews', [])
topNews.directive('appTopnews', function(){
  return{
    restrict: 'E',
    templateUrl: "topNews/topnews.component.html",
    link: function(scope, element, attrs){}
  }
})
topNews.controller('topCtrl', function($http, $scope, topNewsFactory){


  $scope.category = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology' ];
  $scope.country = ["ae", "ar", "at", "au", "be", "bg", "br", "ca", "ch", "cn", "co", "cu", "cz", "de", "eg", "fr", "gb", "gr",
   "hk", "hu", "id", "ie", "il", "in", "it", "jp", "kr", "lt", "lv", "ma", "mx", "my", "ng", "nl", "no", "nz", "ph", "pl", "pt",
    "ro", "rs", "ru", "sa", "se", "sg", "si", "sk", "th", "tr", "tw", "ua", "us", "ve", "za"]

  $scope.fcountry = 'ru';
  $scope.page = 1;
  $scope.fcategory = $scope.category[1]
// https://newsapi.org/v2/top-headlines?country=us&apiKey=eed81886b1b8427d8f75b1aec5837517




  $scope.getData = function(){
    $scope.url = 'https://newsapi.org/v2/top-headlines?page=' + $scope.page + '&category=' + $scope.fcategory + '&country=' + $scope.fcountry + '&apiKey=eed81886b1b8427d8f75b1aec5837517';
    topNewsFactory.getData($scope)
      .then((success)=>{
           console.log('success', success.data);
           $scope.articles = success.data.articles;
      },(error)=>{
          $scope.page = $scope.page - 1;
           alert('последняя страница');
      });
  }

  $scope.inc = function(){
      $scope.page = $scope.page + 1;
      $scope.getData();
      console.log($scope.page);
  }

  $scope.dec = function(){
      if ($scope.page > 1) {
        $scope.page = $scope.page - 1;
        $scope.getData();
      }else{
        alert('нулевая страница');
      }
  }

  $scope.getData();
});;

topNews.factory('topNewsFactory', function($http){
    return {
        getData: function($scope) {
            return $http.get($scope.url)
        }
    };
});
