

// Define the `news` module
let app = angular.module('app', [
  'news',
  'topnews',
  'ngRoute',
  'article',
])

app.config(function($routeProvider){
  $routeProvider
    .when('/top', {
      template: "<app-topnews></app-topnews>"
    })
    .when('/everything', {
      template: "<app-news></app-news>"
    })
    .when('/post/:postId', {
      template: "<app-article></app-article>"
    })
    .otherwise({
      template: "404 no such page"
    })
});
