
let article = angular.module('article', [])
article.directive('appArticle', function(){
  return{
    restrict: 'E',
    templateUrl: "article/article.component.html",
    link: function(scope, element, attrs){
      console.log('directive article');
    }
  }
})
